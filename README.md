# Leeds Exercise

Copyright © 2016, Mohamad Zafranudin Zafrin

Available on Bitbucket - https://bitbucket.org/Xavier-IV/leedslabexercise/

## Setup

You can open the project using Eclipse IDE simply by directing the directory to this folder

## Log

v1.2.0

* Included Exercise 3
* Included Exercise 4
* Fixed some unnecessary import

v1.0.0

* Included Exercise 1
* Included Exercise 2
* Included Practice Exercise from slideshow

	
