package leeds.lab3.dice;

import java.util.Random;
import java.util.Scanner;

public class Dice {

	int dice1NoSide, dice2NoSide;
	int dice1Roll, dice2Roll;
	
	int totalDice1Roll, totalDice2Roll;
	String turn;
	
	Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {

		Dice dc = new Dice();
		dc.dice1NoSide = dc.promptSideDice1();
		dc.dice2NoSide = dc.promptSideDice2();
		
		dc.randomizer(dc.dice1NoSide, dc.dice2NoSide);
	}
	
	public int promptSideDice1(){
		
		System.out.print("How many sides does die 1 have?");
		dice1NoSide = input.nextInt();
		
		return dice1NoSide;
	}
	
	public int promptSideDice2(){
		
		System.out.print("How many sides does die 2 have?");
		dice2NoSide = input.nextInt();
		
		return dice2NoSide;
	}
	
	public void randomizer(int dice1NoSide, int dice2NoSide){
		Random randomize = new Random();
		
		for(int x=0; x<3; x++){
			dice1Roll = randomize.nextInt(dice1NoSide);
			dice2Roll = randomize.nextInt(dice2NoSide);
			
			switch(x){
			case 0:
				turn = "first";
				break;
			case 1:
				turn = "second";
				break;
			default:
				turn = "third";
				
			}
			
			totalDice1Roll += dice1Roll;
			totalDice2Roll += dice2Roll;
			
			System.out.println("Die 1 "+turn+" roll = "+dice1Roll);
			System.out.println("Die 1 "+turn+" roll = "+dice2Roll);
		}
		
		float avgDice1Roll = (float)totalDice1Roll/3;
		float avgDice2Roll = (float)totalDice2Roll/3;
		
		System.out.println("Die 1 rolled a total of "+totalDice1Roll+" and rolled "+avgDice1Roll+" on average");
		System.out.println("Die 2 rolled a total of "+totalDice2Roll+" and rolled "+avgDice2Roll+" on average");
	}
}

