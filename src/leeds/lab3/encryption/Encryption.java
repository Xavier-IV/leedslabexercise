package leeds.lab3.encryption;

import java.util.Random;
import java.util.Scanner;

public class Encryption {

	Scanner input = new Scanner(System.in);
	
	int plainDigit;

	String hexedDigit;
	
	public static void main(String[] args) {
		Encryption enc = new Encryption();
		
		enc.getInput();
		enc.encryptDigit(enc.plainDigit);
		
		System.out.println("Your encrypted pin number is "+enc.hexedDigit);

	}
	
	public int getInput(){
		System.out.print("Enter a 4 digit pin number to encrypt: ");
		
		plainDigit = input.nextInt();
		
		return plainDigit;
	}
	
	public String encryptDigit(int plainDigit){
		
		//Convert to hexadecimal string
		
		hexedDigit = Integer.toHexString(plainDigit);
		
		
		//Generate 2 random number greater than 1000
		Random randomize = new Random();
		Random randomize2 = new Random();
		int digit1 = (int) (randomize.nextInt(plainDigit)) + 1000;
		int digit2 = (int) (randomize2.nextInt(plainDigit)) + 1000;
		
		//Convert these 2 into hexa string
		
		String hexed1 = Integer.toHexString(digit1);
		String hexed2 = Integer.toHexString(digit2);
		
		hexedDigit = hexed1 + hexedDigit + hexed2;
		return hexedDigit;
	}

}
