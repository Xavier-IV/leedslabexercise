package leeds.practice.scanner;

import java.util.Scanner;

public class Scan {
	
	String name, course, sibling, hometown, ambition, quote;
	Scanner sc = new Scanner(System.in);

	public Scan(){
		this.name = "";
		this.course = "";
		this.sibling = "";
		this.hometown = "";
		this.ambition = "";
		this.quote = "";
	}
	
	public static void main(String[] args){
			
		Scan scn = new Scan();
		
		scn.getName();
		scn.getDegree();
		scn.getSibling();
		scn.getHometown();
		scn.getAmbition();
		scn.getQuote();

		scn.printOutput();

		

	}
	
	public void printOutput(){
		System.out.print("<");
		for(int x = 0; x<25;x++){

			System.out.print("-");
		}
		System.out.print(">");
		
		
		System.out.println("\n\nHello " + name + "!");
		System.out.print("Your course is " + course);

		System.out.print(" and you have " + sibling + " awesome sibling(s).\n\n");
		
		
		System.out.println("Your hometown is " + hometown + "\n");
		System.out.println("Your ambition is "+ ambition + ".");
		System.out.print("Your quote is " + quote + "\n\n");
		
		System.out.print("<");
		for(int x = 0; x<25;x++){

			System.out.print("-");
		}
		System.out.print(">");
	}
	
	public String getName(){
		System.out.println("What is your name?");
		
		name = sc.nextLine();
		
		return name;
	}
	
	public String getDegree(){
		System.out.println("What course of degree you are taking?");
		
		System.out.print("For sure it is ");
		course = sc.nextLine();
		
		return course;
	}
	
	public String getSibling(){
		System.out.println("How many sibling you have?");
		sibling = sc.nextLine();
		
		return sibling;
	}
	
	public String getHometown(){
		System.out.println("Which hometown you're living?");
		
		hometown = sc.nextLine();
		
		return hometown;
	}
	
	public String getAmbition(){
		System.out.println("What is your ambition?");
		
		ambition = sc.nextLine();
		
		return ambition;
	}
	
	public String getQuote(){
		System.out.println("What is your quote of life?");
		
		quote = sc.nextLine();
		
		return quote;
	}
	
}
