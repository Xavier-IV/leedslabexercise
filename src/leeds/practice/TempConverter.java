package leeds.practice;

import java.text.DecimalFormat;

public class TempConverter {

	public static void main(String[] args) {
		final int BASE = 32;
		final double CONVERSION_FACTOR  = (float) (9.0 / 5.0);
		
		
		DecimalFormat f = new DecimalFormat("#.00");
		double fahrenheitTemp;
		int celsiusTemp = 24;
		
		fahrenheitTemp = celsiusTemp * CONVERSION_FACTOR + BASE;
		
		System.out.println("Celsius Temperature: "+celsiusTemp);
		System.out.println("Fahrenheit Equivalent: "+f.format(fahrenheitTemp));

	}

}
