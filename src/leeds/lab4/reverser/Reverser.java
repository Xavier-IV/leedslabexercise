package leeds.lab4.reverser;

import java.util.ArrayList;
import java.util.Scanner;

public class Reverser {

	String sentence;
	
	int x = 0;
	int length = 0;
	
	ArrayList<String> split = new ArrayList<String>();
	ArrayList<String> reverse = new ArrayList<String>();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Reverser rv = new Reverser();
		rv.getInput();
		
		//Setting the length of ArrayList
		rv.length = rv.split.size();
		
		//Looping through each ArrayList
		for(int i = 0; i < rv.length; i++){
			
			//Reversing using new StringBuilder
			//Source - http://stackoverflow.com/questions/7569335/reverse-a-string-in-java
			
			rv.reverse.add(new StringBuilder(rv.split.get(i)).reverse().toString());
			
			//Printing out the output
			System.out.print(rv.reverse.get(i) + " ");
		}
		
	}
	
	public void getInput(){

		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		
		System.out.println("Please enter a sentence :");
		sentence = input.nextLine();
		
		//Separating each word using delimiter
		Scanner s = new Scanner(sentence);
		s.useDelimiter(" ");
		
		//Reading each word and storing it into ArrayList
		try{
			while(s.hasNext()){
				split.add(s.next());
			}
		}finally{
			//Close the scanner to protect memory
			s.close();
		}

	}

}
