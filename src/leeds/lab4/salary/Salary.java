package leeds.lab4.salary;

import java.util.Scanner;
import java.text.NumberFormat;

public class Salary
{
   public static void main (String[] args)
   {
       double currentSalary;  // employee's current  salary
       double raise = 0.0;          // amount of the raise
       double newSalary;      // new salary for the employee
       String rating;         // performance rating

       @SuppressWarnings("resource")
       Scanner scan = new Scanner(System.in);

       System.out.print ("Enter the current salary: ");
       currentSalary = scan.nextDouble();
       System.out.print ("Enter the performance rating (Excellent, Good, or Poor): ");
       rating = scan.next();
       
       rating = rating.toLowerCase();
       


       // Compute the raise using if ...
       if(rating.equals("excellent")){
    	   raise = (float)(currentSalary * 0.06);
       }else if (rating.equals("good")){
    	   raise = (float)(currentSalary * 0.04);
       }else if (rating.equals("bad")){
    	   raise = (float)(currentSalary *  0.015);
       }
    	   
       newSalary = currentSalary + raise;

       // Print the results
       NumberFormat money = NumberFormat.getCurrencyInstance();
       System.out.println();
       System.out.println("Current Salary:       " + money.format(currentSalary));
       System.out.println("Amount of your raise: " + money.format(raise));
       System.out.println("Your new salary:      " + money.format(newSalary));
       System.out.println();
    }
}
