package leeds.lab4.count;

import java.io.IOException;

public class Count {

	int total = 0;
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		BufferRead bf = new BufferRead();
		Count cnt = new Count();
		
		bf.bufferedReader("inputCount.txt");
		
		System.out.println("The file has " + bf.n + " value(s).");
		
		cnt.printTable();
		for(int i = 0; i < bf.n; i++){
			System.out.println(bf.splitArrayBufferData(i));
		}
		cnt.endTable();
		
		for(int i = 0; i < bf.n; i++){
			cnt.total += Integer.parseInt(bf.splitArrayBufferData(i));
		}
		
		System.out.println("Total is "+cnt.total);
	}
	
	public void getMessage(){
		System.out.println("Counting from files");
	}
	
	public void printTable(){
		System.out.println("-----------------");
		System.out.println(" Value(s)\t\t");
		System.out.println("-----------------");
	}
	
	public void endTable(){
		System.out.println("-----------------");

	}

}
