package leeds.lab4.count;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class BufferRead {
	BufferedReader inputStream = null;
	Scanner s = null;
	private String[] splitArrayBuffer = new String[10];
	String data = null;
	int n = 0;


	public void bufferedReader(String inputPath) throws IOException	{
		try {
			
			s = new Scanner(new BufferedReader(new FileReader(inputPath)));
			s.useDelimiter(" ");
			

			while(s.hasNext()) {
				data = s.next();
				splitArrayBuffer[n] = data;
				n++;
			}

		} 
		finally {
			if (s != null) {
				s.close();
			}
		}
		
	}
	
	public String splitArrayBufferData(int x) {
		return splitArrayBuffer[x];
	}
	
	public int getCount(){
		int count = splitArrayBuffer.length;
		return count;
	}
	
	public void showData(int x) {
		System.out.print(splitArrayBuffer[x]);
	}
	
	public int arrayLength() {
		return splitArrayBuffer.length;
	}
	
	public int valueN() {
		return n;
	}
}
