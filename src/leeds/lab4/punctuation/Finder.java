package leeds.lab4.punctuation;

public class Finder {
	
	int periodCount;
	private int commaCount;
	private int exclaimCount;
	private int questCount;
	private int dquoteCount;
	private int quoteCount;
	private int bracket1Count;
	private int bracket2Count;
	
	public void getPunctuation(String str){
		Finder p = new Finder();

		
		for(int i = 0; i < str.length(); i++){
			char c = str.charAt(i);
			
			switch(c){
				case '.':
					p.periodCount++;
					break;
				case ',':
					p.commaCount++;
					break;
				case '!':
					p.exclaimCount++;
					break;
				case '?':
					p.questCount++;
					break;
				case '\"':
					p.dquoteCount++;
					break;
				case '\'':
					p.quoteCount++;
					break;
				case '(':
					p.bracket1Count++;
					break;
				case ')':
					p.bracket2Count++;
					break;
				default:
			}
		}
		
		str = str.replaceAll("[A-Za-z0-9 ]", "");
		
		System.out.println("(" + str.length() + ") punctuations found!");
		
		System.out.println("--------------------");
		if(p.periodCount != 0){
			System.out.println("[" + p.periodCount +"] Period ");	
		}
		if(p.commaCount != 0){
			System.out.println("[" + p.commaCount +"] Comma ");
			
		}
		if(p.exclaimCount != 0){
			System.out.println("[" + p.exclaimCount +"] Exclaimation ");
		}
		if(p.questCount != 0){
			System.out.println("[" + p.questCount +"] Question "); 
	
		}
		
		if(p.dquoteCount != 0){
			System.out.println("[" + p.dquoteCount +"] Double Quote ");	
		}
		if(p.quoteCount != 0){
			System.out.println("[" + p.quoteCount +"] Single Quote ");
			
		}
		if(p.bracket1Count != 0){
			System.out.println("[" + p.bracket1Count +"] Left Bracket ");
		}
		if(p.bracket2Count != 0){
			System.out.println("[" + p.bracket2Count +"] Right Bracket "); 
	
		}
		System.out.println("--------------------");
	}
}
