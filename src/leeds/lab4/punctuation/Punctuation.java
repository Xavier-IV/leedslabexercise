package leeds.lab4.punctuation;

import java.io.IOException;

public class Punctuation {

	public static void main(String[] args) throws IOException {
		//Creating object for needed class
		BufferRead bf = new BufferRead();
		Finder find = new Finder();
		
		System.out.println("Reading from \'punctuationFile.txt\'\n");
		
		//Reading from punctuationFile.txt  and returning newLine string
		
		bf.bufferedReader("punctuationFile.txt");		/*BufferRead.java class*/
		
		//Assigning the returned string into str
		String str = bf.newLine;
		
		//Finding the punctuation and outputting the table
		
		find.getPunctuation(str);						/*Finder.java class*/
		
	}
	

	

}