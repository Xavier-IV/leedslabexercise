package leeds.lab4.punctuation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class BufferRead {
    public String newLine = "";

	public String bufferedReader(String inputPath) throws IOException	{
		
		@SuppressWarnings("resource")
		BufferedReader reader = new BufferedReader(new FileReader(inputPath));
	    String line = null;
	    
	    while ((line = reader.readLine()) != null) {
	        newLine += line;
	        
	        
		   }
		
	    return newLine;

	} 

		
}

