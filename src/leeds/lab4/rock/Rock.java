package leeds.lab4.rock;

import java.util.Random;
import java.util.Scanner;

public class Rock {

	String personPlay = "";    //User's play -- "R", "P", or "S"
	String computerPlay = "";  //Computer's play -- "R", "P", or "S"
	int computerInt;      //Randomly generated number used to determine
	                      //computer's play
	
	Scanner scan = new Scanner(System.in);
	Random generator = new Random();
	
	public static void main(String[] args)
	{
		Rock rock = new Rock();
		
		do{
			rock.doAlgorithm();
		}while(!rock.personPlay.equals("Exit"));
		
	}
	
	public void doAlgorithm(){


		//Get player's play -- note that this is stored as a string
		System.out.println("Please choose one[Rock, Paper, Scissor]: ");
		personPlay = scan.nextLine();
		
		//Make player's play uppercase for ease of comparison
		personPlay = personPlay.toUpperCase();
		personPlay = personPlay.substring(0, 1);
		//Generate computer's play (0,1,2)
		
		computerInt = generator.nextInt(3);

		//Translate computer's randomly generated play to string
		switch (computerInt)
		{
			case 0:
				computerPlay = "R";
				break;
			case 1:
				computerPlay = "P";
				break;
			case 2:
				computerPlay = "S";
				break;
			
		}

		//Print computer's play
		//See who won.  Use nested ifs instead of &&.
		System.out.print("[Computer - " + computerPlay + " | " + "You - " + personPlay + "] : ");
		if (personPlay.equals(computerPlay)) { 
		    System.out.println("It's a tie!");
		}
		else if (personPlay.equals("R")){
		    if (computerPlay.equals("S"))
		    	System.out.println("Rock crushes scissors.  You win!!");
		    else
		    	System.out.println("Paper crumples rock. You Lose!!");

		}
		else if(personPlay.equals("P")){
		    if (computerPlay.equals("R"))
		    	System.out.println("Paper crumples rock.  You win!!");
		    else
		    	System.out.println("Scissor cuts paper. You Lose!!");			
		}
		else if(personPlay.equals("S")){
		    if (computerPlay.equals("P"))
		    	System.out.println("Scissor cuts paper.  You win!!");
		    else
		    	System.out.println("Rock breaks scissor. You Lose!!");			
		}
	}
}
