package leeds.lab2.milestokm;

import java.util.Scanner;

public class MilesToKm {
	float miles = 0;
	final float MILES_TO_KM = (float) 1.60935;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MilesToKm mtk = new MilesToKm();
		mtk.promptInput();
		mtk.calculate(mtk.miles);
	}
	
	public float promptInput(){
		System.out.println("Miles to KM converter. Enter your value:");
		
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		
		miles = input.nextFloat();
		
		return miles;
	}
	
	public void calculate(float miles){
		System.out.println("Value to convert - "+miles+" mile(s).");

		float km = (float) (miles * MILES_TO_KM);
		
		System.out.println(miles + " miles = "+km+" km.");
	}
	
	

}
